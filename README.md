# Lab5 -- Integration testing

## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch.

## Bugs

1. Specs say that price per kilometer is 13 while it is actually 12.5
2. Specs say that luxury price per minute is 40 whilte it is actually 28
3. The fixed plan should be available for budget only, but it does not give an invalid response for luxury type.
4. The discount does not work (does not affect the price).
5. If deviation occurs, the price per minute (for both types) changes to 16.666...
6. Service allows for null and zero distances (distance = 0 or distance = null).
7. Service allows for zero or null planned distance.
8. Service allows for zero time resulting in price = 0.
9. Serive allows for null time resulting in price = null.
10. Service allows for zero and null planned time.

## Tables

[Google Sheet Link](https://docs.google.com/spreadsheets/d/1rJIdaG3Kd3ZDvAveBnjrsgelKGpzNorGuo-qR8lqQcI/edit?usp=sharing)

## I wrote some code, check it out ;)

It is in the file tester.js
