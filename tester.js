const axios = require('axios')
const fs = require('fs');

const urlPrefix = 'https://script.google.com/macros/s/AKfycbyVk8l8zITfviXUYtwglrBeaC-YZgZvzYEPad6OK1pJ1ZnjMAvKbxGLlA/exec?service=calculatePrice';
const email = 'a.elbatanony@innopolis.ru';

const invalid = 'Invalid Request';

let single = false; // single request or generated requests
let original = false; // using modified calculations or original calculations

// constants
const budgetPerMin = 17;
const luxuryPerMin = 28;
const pricePerKm = 12.5;
const allowedDeviation = 11;
const discoundValue = 11;

// Current parameters
let type = 'budget';
let plan = 'fixed_price';
let distance = 10;
let plannedDist = 10;
let time = 10;
let plannedTime = null;
let discount = 'no';

let deviation = 0;

// variations (BVA)
let types = ['budget', 'luxury'];
let plans = ['minute', 'fixed_price'];
let numbericRanges = [-10, 0, 10, 100, null];
let distances = numbericRanges;
let plannedDists = numbericRanges;
let times = numbericRanges;
let plannedTimes = numbericRanges;
let discounts = ['yes', 'no', null];

let cache = {};

function generateUrl() {
    const url = `${urlPrefix}\
&email=${email}\
&type=${type}\
&plan=${plan}\
&distance=${distance}\
&planned_distance=${plannedDist}\
&time=${time}\
&planned_time=${plannedTime}\
&inno_discount=${discount}`
    return url;
}

function savedCache() {
    fs.writeFile('cache.json', JSON.stringify(cache), (err) => {
        if (err) throw err;
    });
}

function loadCache() {
    fs.readFile('cache.json', 'utf-8', async (err, data) => {
        if (err) throw err;
        cache = JSON.parse(data.toString());
        if (single) await queryService();
        else await generateQueries();
        savedCache();
    });
}

async function getResult(url) {
    let res = cache[url];
    if (!res) {
        console.warn('Fetching uncached request');
        res = (await axios(url)).data
        cache[url] = res;
        savedCache();
    }
    return res;
}

let mismatchCount = 0;

function mismatch(fetched, expected) {
    console.error(`Expected ${expected}, but received`, fetched);
    console.log(type, plan, distance, plannedDist, time, plannedTime, discount, deviation)
    mismatchCount++;
}

function compareResults(fetched, expected) {
    if (fetched.price == null && expected == null) return;
    if (fetched.price == 0 && expected == 0) return;

    if (fetched.price && expected != invalid) {
        if (Math.abs(fetched.price - expected) > 0.05)
            mismatch(fetched, expected)
    } else if (fetched != expected) mismatch(fetched, expected)
}

async function queryService() {
    const url = generateUrl();
    let res = await getResult(url);
    let expected = original ? originalCalculation() : calculatePrice();
    if (single) console.log(res, expected);
    compareResults(res, expected);

}

function getDeviation(actual, planned) {
    if (actual == null || planned == null) return 100;
    if (actual == 0 || planned == 0) return 100;
    return Math.abs((actual - planned) / planned) * 100;
}

function calculatePrice() {

    if (type == null || plan == null || discount == null)
        return invalid;
    if (distance < 0 || plannedDist < 0 || time < 0 || plannedTime < 0)
        return invalid;

    if (time == null) return null;

    let adjustedPlan = plan;

    /* Serive allows for null distance and plannedDist */
    // if (distance == null || plannedDist == null || time == null) return invalid;

    /* Service allowed fixed_price with luxury */
    // if (plan == 'fixed_price' && type != 'budget') return invalid;

    let budgetMinMultiplier = budgetPerMin;
    let luxuryMinMultiplier = luxuryPerMin;

    deviation = Math.max(getDeviation(time, plannedTime), getDeviation(distance, plannedDist));

    if (deviation > allowedDeviation) {
        /* deviation changes price per minute if was fixed plan */
        if (plan == 'fixed_price') {
            budgetMinMultiplier = 16.66667;
            luxuryMinMultiplier = 16.66667;
        }
        adjustedPlan = 'minute';
    }

    let ret = 0;

    if (adjustedPlan == 'minute') ret += time * (type == 'budget' ? budgetMinMultiplier : luxuryMinMultiplier);
    else ret += distance * pricePerKm;

    /* Service does not use discount */
    // if (discount) ret = ret * ((100 - discoundValue) / 100);

    return ret;
}

/*
Here is InnoCar Specs:
Budet car price per minute = 17
Luxury car price per minute = 40
Fixed price per km = 13
Allowed deviations in % = 11
Inno discount in % = 11
*/

function originalCalculation() {

    if (type == null || plan == null || distance == null || plannedDist == null || time == null || plannedTime == null || discount == null)
        return invalid;
    if (distance <= 0 || plannedDist <= 0 || time <= 0 || plannedTime <= 0)
        return invalid;

    let adjustedPlan = plan;

    if (plan == 'fixed_price' && type != 'budget') return invalid;

    deviation = Math.max(getDeviation(time, plannedTime), getDeviation(distance, plannedDist));

    if (deviation > allowedDeviation) adjustedPlan = 'minute';

    let ret = 0;

    if (adjustedPlan == 'minute') ret += time * (type == 'budget' ? 17 : 40);
    else ret += distance * 12.5;

    if (discount == 'yes') ret = ret * ((100 - discoundValue) / 100);

    return ret;
}

async function generateQueries() {
    console.log('Starting ...')
    for (let i = 0; i < types.length; i++)
        for (let j = 0; j < plans.length; j++)
            for (let k = 0; k < distances.length; k++)
                for (let x = 0; x < plannedDists.length; x++)
                    for (let y = 0; y < times.length; y++)
                        for (let z = 0; z < plannedTimes.length; z++)
                            for (let h = 0; h < discounts.length; h++) {
                                type = types[i];
                                plan = plans[j];
                                distance = distances[k];
                                // plannedDist = plannedDists[x];
                                // time = times[y];
                                // plannedTime = plannedTimes[z];
                                // discount = discounts[h];
                                await queryService();
                            }
    console.log(`Finished with ${mismatchCount} mismatches`);
}

loadCache();